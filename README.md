# ffapp

**f**ire**f**ox **app** creation script for Linux

This script tries to create a Firefox app similar to Chrome

It creates a named Firefox profile which can be launched separately and also a Linux `desktop` file with an icon which can be used to launch the app within the desktop environment.

## Usage

### Creating

``` bash
ffapp create <app name> <web site url>
```

For example:

``` bash
ffapp create twitter https://twitter.com
```

### Deleting

``` bash
ffapp delete <app name>
```

For example:

``` bash
ffapp delete twitter
```

This delete the `desktop` file and the Firefox profile. What is still missing is the remove of the profile entry in `~/.mozilla/firefox/profiles.ini` which ahs to be done manually.


## TODO

- hide tabbar and navigation bar (I'm doing this manually with addons currently)
- activate adblocker
